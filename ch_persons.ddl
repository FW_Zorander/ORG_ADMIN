  CREATE TABLE "CH_PERSONS" 
   (	"NAME_ID" NUMBER GENERATED BY DEFAULT ON NULL AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  NOT NULL ENABLE, 
	"PATERNAL_ID" NUMBER, 
	"MATERNAL_ID" NUMBER, 
	"FIRST_NAME" VARCHAR2(32 CHAR), 
	"MIDDLE_NAMES" VARCHAR2(32 CHAR), 
	"NICKNAME" VARCHAR2(32 CHAR), 
	"LAST_NAME" VARCHAR2(32 CHAR), 
	"MAIDEN_NAME" VARCHAR2(32 CHAR), 
	"IMAGE_FLAG" VARCHAR2(1 CHAR), 
	"CREATED_BY" NUMBER, 
	"CREATE_DATE" DATE, 
	"LAST_UPDATED_BY" NUMBER, 
	"LAST_UPDATE_DATE" DATE, 
	 CONSTRAINT "CH_PERSONS_PK" PRIMARY KEY ("NAME_ID")
  USING INDEX  ENABLE
   ) ;

   COMMENT ON COLUMN "CH_PERSONS"."PATERNAL_ID" IS 'Fathers Record ID';
   COMMENT ON COLUMN "CH_PERSONS"."MATERNAL_ID" IS 'Mothers Record ID';
   COMMENT ON COLUMN "CH_PERSONS"."FIRST_NAME" IS 'First Name of Person';
   COMMENT ON COLUMN "CH_PERSONS"."MIDDLE_NAMES" IS 'Any Middle Names of Person';
   COMMENT ON COLUMN "CH_PERSONS"."NICKNAME" IS 'Preferred Name of Person';
   COMMENT ON COLUMN "CH_PERSONS"."LAST_NAME" IS 'Last Name of Person';
   COMMENT ON COLUMN "CH_PERSONS"."MAIDEN_NAME" IS 'Pre Marriage Name of Women';
   COMMENT ON COLUMN "CH_PERSONS"."IMAGE_FLAG" IS 'Y or N if Person''s Picture in System';
   COMMENT ON COLUMN "CH_PERSONS"."CREATED_BY" IS 'ID of record Creator';
   COMMENT ON COLUMN "CH_PERSONS"."CREATE_DATE" IS 'Date Record Created';
   COMMENT ON COLUMN "CH_PERSONS"."LAST_UPDATED_BY" IS 'ID of Last Person to Update Record';
   COMMENT ON COLUMN "CH_PERSONS"."LAST_UPDATE_DATE" IS 'Date Record Last Updated';
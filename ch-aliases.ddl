  CREATE TABLE "CH_ALIASES" 
   (	"ALIAS_ID" NUMBER GENERATED BY DEFAULT ON NULL AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  NOT NULL ENABLE, 
	"NAME_ID" NUMBER NOT NULL ENABLE, 
	"ALIAS_NAME" VARCHAR2(64 CHAR) NOT NULL ENABLE, 
	"CREATED_BY" NUMBER NOT NULL ENABLE, 
	"CREATE_DATE" DATE NOT NULL ENABLE, 
	"LAST_UPDATED_BY" NUMBER, 
	"LAST_UPDATE_DATE" DATE, 
	 CONSTRAINT "CH_ALIASES_PK" PRIMARY KEY ("ALIAS_ID")
  USING INDEX  ENABLE
   ) ;

   COMMENT ON COLUMN "CH_ALIASES"."NAME_ID" IS 'ID of Person having alias';
   COMMENT ON COLUMN "CH_ALIASES"."ALIAS_NAME" IS 'Other names person is known by';
   COMMENT ON COLUMN "CH_ALIASES"."CREATED_BY" IS 'ID of person who created record';
   COMMENT ON COLUMN "CH_ALIASES"."CREATE_DATE" IS 'Date record was created';
   COMMENT ON COLUMN "CH_ALIASES"."LAST_UPDATED_BY" IS 'ID of person last updating record';
   COMMENT ON COLUMN "CH_ALIASES"."LAST_UPDATE_DATE" IS 'Date record was last updated';